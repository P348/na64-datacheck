#!/bin/bash -e

# verify the free disk space in the NA64 EOS area

export EOS_MGM_URL="root://eospublic.cern.ch"
eos quota /eos/experiment/na64/

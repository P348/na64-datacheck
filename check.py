#!/usr/bin/env python

from pprint import pprint
from collections import OrderedDict

# sorting key in increasing run/chunk order
def keyByRunChunk(item):
    fname = item[0]
    # file name example: cdr01006-002189.dat
    # extract chunk and run parts
    parts = fname.replace("cdr", "").replace(".dat", "").split("-")
    run = parts[1]
    chunk = parts[0]
    return run + chunk

# read listings
print "Reading data:"
data = ["daq-list.txt", "db-list.txt", "eos-list.txt", "castor-list.txt"]
chunks = {}

for fdata in data:
  dataset = fdata.split("-")[0]
  print "Input file = %s, dataset = %s" % (fdata, dataset)
  
  f = open(fdata)
  n = 0
  
  for line in f:
    n+=1
    
    fname, fsize, fsum = line.rstrip("\n").split(" ")
    #print "fname=%s dataset=%s fsize=%s fsum=%s" % (fname, dataset, fsize, fsum)
    
    if not fname in chunks:
      chunks[fname] = {}
      parts = fname.replace("cdr", "").replace(".dat", "").split("-")
      chunks[fname]["run"] = int(parts[1])
      chunks[fname]["chunk"] = int(parts[0][2:])
      #chunks[fname]["fname"] = fname
    
    chunks[fname][dataset] = (fsize, fsum)
  
  print "Entries = %d" % n
  
  f.close()

print "Total entries = %d" % len(chunks)
print

#print chunks
#pprint(chunks, width=1000)

# re-sort by run/chunk order
chunks_sorted = OrderedDict(sorted(chunks.items(), key=keyByRunChunk))

# analyse
db_problem0 = []
db_problem1 = OrderedDict()
db_problem1zero = OrderedDict()
eos_problem0 = []
eos_complete = []
castor_problem0 = []
all_problem = OrderedDict()
all_complete = []

for f, c in chunks_sorted.iteritems():
  isDaq = "daq" in c
  isDb = "db" in c
  isEos = "eos" in c
  isCastor = "castor" in c
  
  if not isDb: db_problem0.append(f)
  if not isEos: eos_problem0.append(f)
  if not isCastor: castor_problem0.append(f)
  
  # check:
  #  - only files existing in DAQ and DB
  #  - compare file size
  #  - DB checksum is not empty
  isDbDaq = isDb and isDaq
  isDbDaqMatch = (not isDbDaq) or ((c["db"][0] == c["daq"][0]) and (c["db"][1] != ""))
  if not isDbDaqMatch: db_problem1[f] = c
  
  # check for incomplete DB entry:
  #  - size = 0
  #  - checksum = ''
  isDbZero = (not isDbDaqMatch) and (c["db"][0] == '0') and (c["db"][1] == '')
  if isDbZero: db_problem1zero[f] = c
  
  # check:
  #  - DB = DAQ = EOS = CASTOR
  #  - DAQ entries have only file size
  isAllMatch = (isDb) and (c["db"][1] != "") and \
               ((not isEos) or (c["db"] == c["eos"])) and \
               ((not isCastor) or (c["db"] == c["castor"])) and \
               ((not isDaq) or (c["db"][0] == c["daq"][0]))
  if not isAllMatch: all_problem[f] = c
  
  # check:
  #  - all DAQ data have proper copy in DB = DAQ = EOS
  isEosComplete = (isDb and isDaq and isEos) and \
                  (c["db"] == c["eos"]) and \
                  (c["db"][0] == c["daq"][0])
  if isEosComplete: eos_complete.append(f)
  
  # check:
  #  - all DAQ data have proper copy in DB = DAQ = EOS = CASTOR
  isDaqComplete = (isDb and isDaq and isEos and isCastor) and \
                  (c["db"] == c["eos"]) and \
                  (c["db"] == c["castor"]) and \
                  (c["db"][0] == c["daq"][0])
  if isDaqComplete: all_complete.append(f)


# print array summary
def array_summary_info(x, detail = False):
    totallen = len(x)
    xstr = ""
    if (totallen < 6) or detail :
      xstr = " ".join(x)
    else :
      xstr = "%s ... %s" % (x[0], x[totallen-1])
    return "%d entries: %s" % (totallen, xstr)

# report
print "Missing entries:"
print "DB missing", array_summary_info(db_problem0)
print "EOS missing", array_summary_info(eos_problem0)
print "CASTOR missing", array_summary_info(castor_problem0)
print

print "DB problematic entries in DB vs DAQ:"
for f, c in db_problem1.iteritems(): print f, c
print "DB mismatch", array_summary_info(db_problem1.keys())
print "DB incomplete", array_summary_info(db_problem1zero.keys(), True)
print

print "ALL problematic entries:"
for f, c in all_problem.iteritems(): print f, c
print "ALL mismatch", array_summary_info(all_problem.keys())
print

print "EOS complete entries:"
print "EOS complete", array_summary_info(eos_complete, True)
print

# all-complete list is disabled as the CASTOR backup is not operational
#print "Complete entries (save to delete from DAQ):"
#print "ALL complete", array_summary_info(all_complete, True)
#print

print "Summary:"
ntotal = len(chunks)
nproblem = len(all_problem)
nmatch = ntotal - nproblem
nmissing = len(db_problem0) + len(eos_problem0) + len(castor_problem0)
miss_distinct = list(set().union(db_problem0, eos_problem0, castor_problem0))

print "Total files:", ntotal
print "Full match files:", nmatch
print "Missing files (all):", nmissing
print "Missing files (distinct):", len(miss_distinct)
print "Problematic files:", nproblem
print "Complete files:", len(all_complete)

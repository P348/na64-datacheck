#!/bin/bash

# strict error checking
set -e -u -o pipefail

flist="$(pwd)/castorcp.txt"
rsync rsync://pcdmre01.cern.ch/data-cdr/ | awk '{print $5}' | grep 'cdr.*dat' | sort -t- -k2 -k1 > $flist
nfiles=$(wc -l $flist | awk '{print $1}')
echo "Total files = $nfiles"

tmpd=$(mktemp -d)
cd $tmpd
echo "Work dir = $(pwd)"

export STAGE_HOST=castorpublic
n=0

while read f ; do
  let n+=1
  dstf="/castor/cern.ch/na64/data/cdr/$f"
  echo "===> $n/$nfiles : $f -> $dstf"
  
  if nsls "$dstf" &> /dev/null ; then continue ; fi
  
  #rsync --bwlimit=20M --progress --archive rsync://pcdmre01.cern.ch/data-cdr/$f .
  rsync --progress --archive rsync://pcdmre01.cern.ch/data-cdr/$f .
  #rsync --block-size=131072 --progress --archive rsync://pcdmre01.cern.ch/data-cdr/$f .
  xrdcp -f $f "root://castorpublic.cern.ch/$dstf"
  modtime=$(date -r $f +%Y%m%d%H%M)
  echo "modtime $modtime"
  nstouch -c -m -t $modtime "$dstf"
  rm -f $f
done < $flist

rm -rf $tmpd

echo "Complete"

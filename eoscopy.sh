#!/bin/bash

# strict error checking
set -e -u -o pipefail

daqsrc="rsync://pcdmre01.cern.ch/data-cdr"
eosdst="/eos/experiment/na64/data/cdr"
bwlimit="40M"
nlimit="200"

echo "[$(date)] Synchronize existing files"
rsync --stats --itemize-changes --archive --size-only --existing "$daqsrc/*.dat" "$eosdst/"

echo "[$(date)] Enumerate new files"
list=$(pwd)/copy.txt
# 'grep||true' is to avoid error exit in case of no-match (empty list)
rsync --stats --itemize-changes --archive --size-only --dry-run "$daqsrc/*.dat" "$eosdst/" | \
  { grep "^>" || true ; } | \
  cut -d " " -f 2 > $list

# default list order is #chunk,#run
# resort list by #run,#chunk order to copy full runs first
sort -t- -k2 -k1 -o $list $list

# print copy list summary
wc -l $list
#du -cb $(cat $list) | tail -n 1 | awk '{print $2, $1/1e9, "Gb"}'

if [[ "$nlimit" != "" ]] ; then
  cat "$list" | head -n "$nlimit" > "$list.nlimit"
  mv "$list.nlimit" "$list"
  wc -l $list
fi

echo "[$(date)] Copy new files"

tmpd=$(mktemp -d)
cd $tmpd
echo "Work dir = $(pwd)"

export EOS_MGM_URL="root://eospublic.cern.ch"

cat $list | while read i ; do
  n=${n-0}
  let n+=1
  dstf="$eosdst/$i"
  echo "====> (file #$n) $i -> $dstf"
  
  if eos ls "$dstf" &>/dev/null ; then
    echo "WARNING: destination file exists already, skip copy"
    continue
  fi
  
  rsync --bwlimit="$bwlimit" --progress --archive "$daqsrc/$i" .
  
  # copy file, eos options:
  #   -p - create parent dirrectory
  #   -P - preserve file time
  #   -S - print summary
  eos cp -p -P "$i" "$dstf"
  # sync metadata
  rsync --itemize-changes --archive --size-only --existing "$daqsrc/$i" "$dstf"
  
  rm -f ./$i
done

cd
rm -rf $tmpd


echo "[$(date)] Complete"

#!/bin/bash -e

# print DAQ current status

url="http://pcdmfs01.cern.ch/api/api.php?name=lastspill"
lastspill=$(wget -q -O - "$url")

echo "$lastspill"

run=$(echo "$lastspill" | grep "^run=" | cut -d = -f 2)
t0=$(echo "$lastspill" | grep "^created=" | cut -d = -f 2)
t0=$(date --date="$t0" +%s)
tnow=$(date +%s)
telaps=$((tnow-t0))
echo "telaps=$telaps"

mode="run"

if (( run > 900000 )) ; then
  mode="dryrun"
fi

if (( telaps > 180 )) ; then
  mode="idle"
fi

echo "mode=$mode"

diskactive="0"
if [[ "$mode" == "run" ]] ; then
  diskactive="1"
fi

echo "diskactive=$diskactive"

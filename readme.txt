Data copy to EOS
================

1. Dedicated user account with write-access to na64 eos data area:

  $ ssh na64eos@lxplus

2. Copy all new data files:

  $ ./eoscopy.sh

3. Repeat (2.) endlessly:

  $ ./eosloop.sh


Data copy to CASTOR
===================

NOTE: as of 2022-may castorcp.sh doesn't work as it uses obsolete castor copy commands

1. Dedicated user account with write-access to na64 castor data area:

  $ ssh p348data@lxplus

2. Copy all new data files:

  $ ./castorcp.sh


Data copy verification
======================

1. Generate listings of data files (name, size, checksum) "*-list.txt":

  $ ./harvest.sh

2. Check consistency:

  $ ./check.py


Reference
---------

Kerberos ticket renew:
  https://twiki.cern.ch/twiki/bin/view/Main/Kerberos
  https://gist.github.com/delannoy/9512a180dea2a3436b13d860bcaf4296

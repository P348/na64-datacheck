#!/bin/bash -e

# xrdadler32 tool:
# $ ssh pcdmfs01
# $ yum install xrootd-client


echo "[$(date)] ===> DAQ / daq@pcdmfs01"

filelist="$@"

# connect to server and calculate files checksum and size
( ssh daq@pcdmfs01 bash -e | tee incomplete-fix.sql ) <<EOT

for i in $filelist ; do
  fname="/data/cdr/\$i"
  fsize=\$(du -b \$fname | awk '{print \$1}')
  fsum=\$(xrdadler32 \$fname | awk '{print \$1}')
  
  #echo "\$i \$fsize \$fsum"
  echo "UPDATE CDR_data_files SET Size = \$fsize, CheckSum = '\$fsum' WHERE FileName = '\${i/cdr01/cdr1}' AND Size = 0 AND CheckSum IS NULL;"
done

EOT

echo "[$(date)] done"
echo

echo "INFO: see output in file incomplete-fix.sql, usage:"
echo "      $ scp incomplete-fix.sql daq@pcdmfs01:/tmp"
echo "      $ ssh daq@pcdmfs01"
echo "      $ mysql -p -u daq DM_DAQ_CONF < /tmp/incomplete-fix.sql"
echo "      $ rm -f /tmp/incomplete-fix.sql"
echo "      $ exit"

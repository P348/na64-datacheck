#!/bin/bash -e

# prepare list of files at DAQ, DB, CASTOR, EOS in format:
#  filename filesize checksum

#echo "[$(date)] ===> DAQ / daq@pcdmfs01"
#ssh daq@pcdmfs01 "find /data/cdr -type f -name '*.dat' -printf '%f %s 0\n'" > daq-list.txt

rloc="rsync://pcdmre01/data-cdr"
echo "[$(date)] ===> DAQ / $rloc"
rsync $rloc | tr -d ',' | grep '\.dat$' | awk '{print $5, $2, 0}' > daq-list.txt


echo "[$(date)] ===> DB (~seconds)"
wget -q -O db-0.txt "http://pcdmfs01.cern.ch/runslist/runslist.php?period=all&sizeunits=bytes"
links -dump -dump-width 1000 -no-numbering -no-references db-0.txt | grep -o '/data/cdr/cdr.*' | cut -d ' ' -f 1,3- | cut -d / -f 4- | awk '{print $1, $2, $3}' > db-list.txt

# workaround for DB corruption, broken entries/fields returned as '?'
#cat db-list.txt | tr '?' 0 > db-list1.txt
#mv -f db-list1.txt db-list.txt


echo "[$(date)] ===> CASTOR (~10seconds)"
#nsls -l --checksum /castor/cern.ch/na64/data/cdr/ > castor-0.txt
#cat castor-0.txt | cut -c 32-51,69-76,78- --output-delimiter=: | while IFS=': ' read fsize fsum fname ; do echo "$fname $fsize $fsum" ; done > castor-1.txt
#cat castor-1.txt | while read fname fsize fsum ; do if [[ "$fsum" == "" && "$fsize" == "0" ]] ; then fsum="1"; fi ; addlen=$((8-${#fsum})) ; if [[ "$addlen" != "0" ]] ; then fsum=$(printf "%0${addlen}d%s" 0 $fsum); fi ; echo "$fname $fsize $fsum" ; done > castor-list.txt
: > castor-list.txt


echo "[$(date)] ===> EOS (~minutes)"
export EOS_MGM_URL="root://eospublic.cern.ch"

#eos ls /eos/experiment/na64/data/cdr | sort -t- -k2 -k1 > eos-0.txt
#{
#  echo "cd /eos/experiment/na64/data/cdr"
#  cat eos-0.txt | while read line ; do echo "fileinfo $line -m" ; done
#} > eos-1.eosh

# takes a few minutes to complete
#eos -b eos-1.eosh > eos-2.txt

#cat eos-2.txt | cut -d ' ' -f 2,3,17 | tr = ' ' | cut -d ' ' -f 2,4,6 | cut -d / -f 7- > eos-3.txt
#cat eos-3.txt | while read fname fsize fsum ; do if [[ "$fsum" == "00000000" && "$fsize" == "0" ]] ; then fsum="00000001"; fi ; echo "$fname $fsize $fsum" ; done > eos-list.txt


eos newfind -f --size --checksum /eos/experiment/na64/data/cdr > eos-2.txt

cat eos-2.txt | awk '{print $1, $2, $3}' | tr = ' ' | cut -d ' ' -f 1,3,5 | cut -d / -f 7- > eos-3.txt
# correct corrupted checksums of size=0 files
#cat eos-3.txt | while read fname fsize fsum ; do if [[ "$fsize" == "0" ]] ; then fsum="00000001"; fi ; echo "$fname $fsize $fsum" ; done > eos-list.txt
cp eos-3.txt eos-list.txt


echo "[$(date)] harvesting done"

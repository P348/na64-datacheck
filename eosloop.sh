#!/bin/bash

nfail="0"

while true ; do
  echo "[$(date)] => renew kerberos ticket..."
  klist
  kinit -R
  klist
  
  echo "[$(date)] => COPY..."
  ./eoscopy.sh
  
  if [[ "$?" != "0" ]] ; then
    let nfail+=1
    echo "WARNING: nfail=$nfail"
  else
    nfail="0"
  fi
  
  if [[ "$nfail" == "3" ]] ; then
    echo "ERROR: the eoscopy.sh failed 3 times, exit"
    
    # send email notification
    subj="error: $0 failed"
    to="$(whoami)@cern.ch"
    msg="$(whoami)@$(hostname), $PWD, $0, error=nfail3"
    echo "$msg" | mail -s "$subj" "$to"
    
    exit 1
  fi
  
  # reduce to 2min in case of retry
  delay="300"
  if [[ "$nfail" != "0" ]] ; then
    delay="120"
  fi
  
  echo "[$(date)] => PAUSE ${delay}s ..."
  sleep ${delay}
  echo ""
done
